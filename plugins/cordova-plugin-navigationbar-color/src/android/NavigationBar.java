/*
 * Copyright (c) 2016 by Vinicius Fagundes. All rights reserved.
 *
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apache License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * http://opensource.org/licenses/Apache-2.0/ and read it before using this
 * file.
 *
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 *
 */

package com.viniciusfagundes.cordova.plugin.navigationbar;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.LOG;
import org.apache.cordova.PluginResult;
import org.json.JSONException;

public class NavigationBar extends CordovaPlugin {
    private static final String TAG = "NavigationBar";

    /**
     * Sets the context of the Command. This can then be used to do things like
     * get file paths associated with the Activity.
     *
     * @param cordova The context of the main Activity.
     * @param webView The CordovaWebView Cordova is running in.
     */
    @Override
    public void initialize(final CordovaInterface cordova, CordovaWebView webView) {
        
        LOG.e("LukeLauncher", "Navbar Color Plugin");
        
        super.initialize(cordova, webView);

        this.cordova.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Clear flag FLAG_FORCE_NOT_FULLSCREEN which is set initially
                // by the Cordova.
                Window window = cordova.getActivity().getWindow();
                window.clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

				settrans("set_trans1");
				
                // Read 'NavigationBarBackgroundColor' and 'NavigationBarLigth' from config.xml, default is #000000.
                //setNavigationBarBackgroundColor(preferences.getString("NavigationBarBackgroundColor", "#000000"), preferences.getBoolean("NavigationBarLigth", false));
            }
        });
    }

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action          The action to execute.
     * @param args            JSONArry of arguments for the plugin.
     * @param callbackContext The callback id used when calling back into JavaScript.
     * @return True if the action was valid, false otherwise.
     */
    @Override
    public boolean execute(final String action, final CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
        
        final Activity activity = this.cordova.getActivity();
        final Window window = activity.getWindow();

        if ("_ready".equals(action)) {
            boolean navigationBarVisible = (window.getAttributes().flags & WindowManager.LayoutParams.FLAG_FULLSCREEN) == 0;
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, navigationBarVisible));
            return true;
        }

        if ("show".equals(action)) {
            this.cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // SYSTEM_UI_FLAG_FULLSCREEN is available since JellyBean, but we
                    // use KitKat here to be aligned with "Fullscreen"  preference
                    /*
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        int uiOptions = window.getDecorView().getSystemUiVisibility();
                        uiOptions &= ~View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
                        uiOptions &= ~View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

                        window.getDecorView().setSystemUiVisibility(uiOptions);

                        window.getDecorView().setOnFocusChangeListener(null);
                        window.getDecorView().setOnSystemUiVisibilityChangeListener(null);
                    }
					*/
                    // CB-11197 We still need to update LayoutParams to force navigation bar
                    // to be hidden when entering e.g. text fields
                    //window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                }
            });
            return true;
        }

        if ("hide".equals(action)) {
            this.cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // SYSTEM_UI_FLAG_FULLSCREEN is available since JellyBean, but we
                    // use KitKat here to be aligned with "Fullscreen"  preference
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        final int uiOptions = window.getDecorView().getSystemUiVisibility()
                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

                        window.getDecorView().setSystemUiVisibility(uiOptions);

                        window.getDecorView().setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if (hasFocus) {
                                    window.getDecorView().setSystemUiVisibility(uiOptions);
                                }
                            }
                        });

                        window.getDecorView().setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                            @Override
                            public void onSystemUiVisibilityChange(int visibility) {
                                window.getDecorView().setSystemUiVisibility(uiOptions);
                            }
                        });
                    }

                    // CB-11197 We still need to update LayoutParams to force navigation bar
                    // to be hidden when entering e.g. text fields
                    //window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                }
            });
            return true;
        }

        if ("settrans".equals(action)) {
            this.cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        settrans(args.getString(0));
                    } catch (JSONException ignore) {
                    }
                }
            });
            return true;
        }

        return false;
    }

public static void setWindowFlag(Window winin, final int bits, boolean on) {
	//Window win = activity.getWindow();
	WindowManager.LayoutParams winParams = winin.getAttributes();
	if (on) {
		winParams.flags |= bits;
	} else {
		winParams.flags &= ~bits;
	}
	winin.setAttributes(winParams);
}


    private void settrans(final String ine) {
	
		
        if (Build.VERSION.SDK_INT >= 21) {
            if (ine != null && !ine.isEmpty()) {
                final Window window = cordova.getActivity().getWindow();
                int uiOptions = window.getDecorView().getSystemUiVisibility();
                             
                // 0x80000000 FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS
                // 0x00000010 SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR

                uiOptions = uiOptions | 0x80000000;

                if(Build.VERSION.SDK_INT >= 26)
                {
                    uiOptions = uiOptions | 0x00000010;
                }
                else
                {
                    uiOptions = uiOptions & ~0x00000010;
				}
                
                try 
                {
                    //Navbar...
					if(ine.equals("set_nav"))
                    {
						window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);				
					}
					
					//Different Statusbar types => different flags 
                    if(ine.equals("set_trans1"))
                    {
						window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
					}
					
					
					if(ine.equals("set_trans2"))
                    {				
						window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
					}		

					if(ine.equals("unset_trans2"))
                    {				
						window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
						window.getDecorView().setPadding(0,0,0,0);
					}	
						
					if(ine.equals("unset_trans1"))
                    {
						window.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
						window.getDecorView().setPadding(0,0,0,0);
					}	
					
					if(ine.equals("set_padding"))
                    {				
						Context context=this.cordova.getActivity().getApplicationContext();
						int statusBarHeight = 0;
						int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
						if (resourceId > 0) 
						{
							statusBarHeight = context.getResources().getDimensionPixelSize(resourceId);
						}
						window.getDecorView().setPadding(0,statusBarHeight,0,0);
					}
					
					if(ine.equals("unset_padding"))
                    {				
						window.getDecorView().setPadding(0,0,0,0);
					}
					
                } catch (IllegalArgumentException ignore) {

                } catch (Exception ignore) { }
            }
        }
    }
}
